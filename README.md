# [WIP] wm-configs 

My basic window manager config (+ vim).

**Warning : the setup script WILL override any current config without backuping first**

Meant to run on standard desktop Archlinux (no wlan / no battery / AZERTY keyboard). 

Run `./setup.py` as non-root user to install required packages (from Arch community repo and AUR). AUR packages are installed "manually" using the aur git repository of the PKGBUILD (which are cloned in a directory inside the current user home directory). Note that `pacman` is run with `sudo` so a password prompt is to be expected.

Configuration files are found in -- *surprise* -- **configs/**.

We also need a little bit of templating for the following things :

- main net interface name (+ wifi if needed ?)
- wallpaper location
- list of mount points for disk usage

Other todos :

- debug the sound menu in polybar (kinda useless currently)
- add nm-cli to polybar (?)
- Still an issue with fonts (system wide / URxvt / polybar). It's currently working but probably not correctly automated.

## Debug

- May need to receive new gpg keys when installing AUR packages (had the problem with Unifonts). Use `gpg --recv-key <KEY_ID>` (with caution)

## Refs & links

- vimrc is a modified version of https://github.com/amix/vimrc basic.vim
- Polybar : https://github.com/polybar/polybar/wiki
- Vim plugins : https://vimawesome.com/
- Rofi : https://wiki.archlinux.org/index.php/Rofi


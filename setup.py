#!/usr/bin/env python3

""" Bootstrap i3/polybar/rofi/... environment.

    Runs for my Archlinux desktop (no battery/wlan
    modules for polybar), on AZERTY keyboard.

    Will install packages (from both Arch community
    repository and AUR), and copy dot files in current
    user's home (no templating on config files, some
    manual changes may be needed).
"""

import os
from shutil import copy
from subprocess import run


INIT_DIR = os.getcwd()

PACKAGES = [
    "i3-gaps",
    "rofi",  # alt $mod+d menu
    "feh",  # set backgrounds
    "terminator",  # default $mod+Return terminal
    "picom",  # compton fork (transparency)
    "arandr",  # display settings
    "mpd",  # music player daemon
    "ncmpcpp",  # mpd client
    "vim",
    "base-devel",
    "xorg-server",
    "xorg-xev",
    "xorg-xinit",
    "python-pip",
    "thunar",
    "thunar-volman",
    "thunar-archive-plugin",
    "thunar-media-tags-plugin",
    "networkmanager",
    "network-manager-applet",
    "nm-connection-editor",
    "dhcpcd",
]

AUR_CLONE_PATH = os.path.expanduser("~/Git-Apps")

AUR_PACKAGES = {
    "Polybar": "https://aur.archlinux.org/polybar.git",
    "Siji": "https://aur.archlinux.org/siji-git.git",
    "Unifont": "https://aur.archlinux.org/ttf-unifont.git",
}

ROOT_CFG = os.path.expanduser("~/.config")
POLYBAR_CFG = f"{ROOT_CFG}/polybar"
I3_CFG = f"{ROOT_CFG}/i3"
ROFI_CFG = f"{ROOT_CFG}/rofi"
MPD_CFG = f"{ROOT_CFG}/mpd"

VIM_CFG = os.path.expanduser(f"~/.vim")


####################################################################################################
##################################### INSTALL_* FUNCTIONS ##########################################
####################################################################################################


def install_pkg():
    """Install pkg from Arch Community repositories"""

    keyrng = run(["sudo", "pacman-key", "--init"])
    keyrng = run(["sudo", "pacman-key", "--populate", "archlinux"])

    print("############################## ARCH COMMUNITY")
    print("#############################################")

    for pkg in PACKAGES:

        print(f"## Installing {pkg} ...")
        cp = run(["sudo", "pacman", "-S", "--noconfirm", "--needed", pkg])

        if cp.returncode == 0:
            print("==> OK")
        else:
            print(f"!!! Error while installing {pkg}. Aborting")
            exit(1)


def install_aur_pkg():
    """Manually make and install pkg from AUR Git repositories"""

    print("######################################### AUR")
    print("#############################################")

    if not os.path.isdir(AUR_CLONE_PATH):
        os.mkdir(AUR_CLONE_PATH, mode=0o755)

    for pkg, git_url in AUR_PACKAGES.items():
        print(f"## Installing {pkg} ...")
        cp = run(["git", "clone", git_url, f"{AUR_CLONE_PATH}/{pkg}"])
        if cp.returncode != 0:
            # We'll try a git pull, assuming the repo already exists
            print(f"Git pull on {AUR_CLONE_PATH}/{pkg}")
            os.chdir(f"{AUR_CLONE_PATH}/{pkg}")
            run(["git", "pull"])
            os.chdir(f"{INIT_DIR}")

        # Build / instal package
        os.chdir(f"{AUR_CLONE_PATH}/{pkg}")
        cp = run(["makepkg", "-sic", "--noconfirm"])

        if cp.returncode == 0:
            print("==> OK")
        else:
            print(f"!!! Error while installing {pkg}. Aborting")
            exit(1)

    run(["fc-cache"])  # because we install some fonts for polybar


def install_deps():
    """Install all packages"""

    install_pkg()

    install_aur_pkg()


####################################################################################################
####################################### SETUP_* FUNCTIONS ##########################################
####################################################################################################


def setup_polybar():
    """Create directories (if necessary) and copy config files for Polybar"""

    if not os.path.isdir(POLYBAR_CFG):
        os.mkdir(POLYBAR_CFG, mode=0o755)

    copy("./configs/polybar/config", f"{POLYBAR_CFG}/config")
    copy("./configs/polybar/launch.sh", f"{POLYBAR_CFG}/launch.sh")


def setup_i3():
    """Create directories (if necessary) and copy config files for i3"""

    if not os.path.isdir(I3_CFG):
        os.mkdir(I3_CFG, mode=0o755)

    copy("./configs/i3/config", f"{I3_CFG}/config")


def setup_rofi():
    """Create directories (if necessary) and copy config files for rofi"""

    if not os.path.isdir(ROFI_CFG):
        os.mkdir(ROFI_CFG, mode=0o755)

    copy("./configs/rofi/config", f"{ROFI_CFG}/config")
    copy("./configs/rofi/slate-custom.rasi", f"{ROFI_CFG}/slate-custom.rasi")


def setup_xresources():
    """Create or replace an ~/.XResource file for urxvt config"""

    copy("./configs/Xresources", os.path.expanduser("~/.Xresources"))
    run(["xrdb", os.path.expanduser("~/.Xresources")])


def setup_mpd():
    """Create directories (if necessary) and copy config files for mpd/ncmpcpp"""

    if not os.path.isdir(MPD_CFG):
        os.mkdir(MPD_CFG, mode=0o755)

    if not os.path.isdir(f"{MPD_CFG}/playlists"):
        os.mkdir(f"{MPD_CFG}/playlists", mode=0o755)

    run(["sudo", "systemctl", "enable", "mpd"])
    run(["sudo", "systemctl", "start", "mpd"])


def clone_or_pull(repo):
    """Clone repo on current path or attempt to pull from repo if clone failed"""

    res = run(["git", "clone", "--depth=1", repo])
    if res.returncode != 0:
        repo_dir = repo.split("/")[-1]
        os.chdir(f"./{repo_dir}")
        run(["git", "pull"])
        os.chdir("..")


def setup_vim():
    """Create .vim / .vim/autoload ./vim/bundle directory if needed
    Install pathogen
    Copy vimrc and other vim configuration files to their rightful place
    Installs a few plugins
    """

    if not os.path.isdir(VIM_CFG):
        os.mkdir(VIM_CFG, mode=0o700)

    if not os.path.isdir(f"{VIM_CFG}/autoload"):
        os.mkdir(f"{VIM_CFG}/autoload", mode=0o700)

    if not os.path.isdir(f"{VIM_CFG}/bundle"):
        os.mkdir(f"{VIM_CFG}/bundle", mode=0o700)

    if not os.path.isdir(f"{VIM_CFG}/colors"):
        os.mkdir(f"{VIM_CFG}/colors", mode=0o700)

    if not os.path.isdir(f"{VIM_CFG}/syntax"):
        os.mkdir(f"{VIM_CFG}/syntax", mode=0o700)

    run(
        [
            "curl",
            "-LSso",
            f"{VIM_CFG}/autoload/pathogen.vim",
            "https://tpo.pe/pathogen.vim",
        ]
    )
    copy("./configs/vim/vimrc", os.path.expanduser("~/.vimrc"))
    copy("./configs/vim/monokai.vim", f"{VIM_CFG}/colors/monokai.vim")
    copy("./configs/vim/yang.vim", f"{VIM_CFG}/syntax/yang.vim")
    copy("./configs/vim/cisco.vim", f"{VIM_CFG}/syntax/cisco.vim")

    os.chdir(f"{VIM_CFG}/bundle")
    clone_or_pull("https://github.com/ambv/black")
    clone_or_pull("https://github.com/vim-syntastic/syntastic")
    clone_or_pull("https://github.com/airblade/vim-gitgutter")
    clone_or_pull("https://github.com/scrooloose/nerdtree")
    clone_or_pull("https://github.com/vim-airline/vim-airline")
    clone_or_pull("https://github.com/vim-airline/vim-airline-themes")
    clone_or_pull("https://github.com/davidhalter/jedi-vim")
    clone_or_pull("https://github.com/plasticboy/vim-markdown")

    # System wide jedi installation for jedi-vim plugin
    run(["sudo", "pip", "install", "jedi"])


def enable_nm():
    """Enable Network Manager"""
    run(["sudo", "systemctl", "enable", "NetworkManager.service"])
    run(["sudo", "systemctl", "start", "NetworkManager.service"])


def setup_cfg():
    """Setup directories and config files for the whole wm"""

    os.chdir(INIT_DIR)
    if not os.path.isdir(ROOT_CFG):
        os.mkdir(ROOT_CFG, mode=0o700)

    setup_polybar()

    setup_i3()

    setup_rofi()

    # setup_xresources()

    setup_mpd()

    setup_vim()


if __name__ == "__main__":

    install_deps()

    setup_cfg()

# i3 (gaps) config 
# i3 config file (v4)

# Display/behaviour preferences and i3-gaps specific
default_border none
default_floating_border none
for_window [class=".*"] border pixel 0
gaps inner 10
focus_follows_mouse no
workspace_auto_back_and_forth yes
# smart_gaps on

## SET instructions

# Use Windows/'Super' key as modifier
set $mod Mod4
floating_modifier $mod

# Workspaces
set $ws1    "1: Fry"
set $ws2    "2: Leela"
set $ws3    "3: Farnsworth"
set $ws4    "4: Bender"
set $ws5    "5: Amy"
set $ws6    "6: Scruffy"
set $ws7    "7: Zoidberg"
set $ws8    "8: Kiff"
set $ws9    "9: Hermes"
set $ws10   "10: Nibbler"

# Not sure about that, needs to be sorted out...
font xft:Inconsolata\ for\ Powerline 
font pango:InconsolataForPowerline 
font pango:monospace 8

## BINDSYM INSTRUCTIONS

## Main bindings
bindsym $mod+Shift+q        kill
bindsym $mod+d exec         "rofi -combi-modi window,drun -show combi -modi combi -show-icons"
bindsym $mod+Return         exec terminator
bindsym $mod+twosuperior    exec urxvt
bindsym $mod+Shift+c        reload
bindsym $mod+Shift+r        restart

# Focus (Vim-like hjkl and arrowkeys)
bindsym $mod+a              focus parent
bindsym $mod+h              focus left
bindsym $mod+j              focus down
bindsym $mod+k              focus up
bindsym $mod+l              focus right
bindsym $mod+Left           focus left
bindsym $mod+Down           focus down
bindsym $mod+Up             focus up
bindsym $mod+Right          focus right

# Move focused (Vim-like hjkl and arrowkeys)
bindsym $mod+Shift+h        move left
bindsym $mod+Shift+j        move down 
bindsym $mod+Shift+k        move up
bindsym $mod+Shift+l        move right
bindsym $mod+Shift+Left     move left
bindsym $mod+Shift+Down     move down
bindsym $mod+Shift+Up       move up
bindsym $mod+Shift+Right    move right

# Split
bindsym $mod+g              split h 
bindsym $mod+v              split v

# Fullscreen
bindsym $mod+f fullscreen   toggle

# Change layout
bindsym $mod+s              layout stacking
bindsym $mod+w              layout tabbed
bindsym $mod+e              layout toggle split

# Tiling / floating settings
bindsym $mod+Shift+space    floating toggle
bindsym $mod+space          focus mode_toggle

# Got to workspace X
bindsym $mod+1              workspace $ws1
bindsym $mod+2              workspace $ws2
bindsym $mod+3              workspace $ws3
bindsym $mod+4              workspace $ws4
bindsym $mod+5              workspace $ws5
bindsym $mod+6              workspace $ws6
bindsym $mod+7              workspace $ws7
bindsym $mod+8              workspace $ws8
bindsym $mod+9              workspace $ws9
bindsym $mod+0              workspace $ws10

# Move to workspace X
bindsym $mod+Shift+1        move container to workspace $ws1
bindsym $mod+Shift+2        move container to workspace $ws2
bindsym $mod+Shift+3        move container to workspace $ws3
bindsym $mod+Shift+4        move container to workspace $ws4
bindsym $mod+Shift+5        move container to workspace $ws5
bindsym $mod+Shift+6        move container to workspace $ws6
bindsym $mod+Shift+7        move container to workspace $ws7
bindsym $mod+Shift+8        move container to workspace $ws8
bindsym $mod+Shift+9        move container to workspace $ws9
bindsym $mod+Shift+0        move container to workspace $ws10

# Resize windows / containers
mode "resize" {

    # Vim like hjkl
    bindsym h               resize shrink width 10 px or 10 ppt
    bindsym j               resize grow height 10 px or 10 ppt
    bindsym k               resize shrink height 10 px or 10 ppt
    bindsym l               resize grow width 10 px or 10 ppt

    # Arrow keys
    bindsym Left            resize shrink width 10 px or 10 ppt
    bindsym Down            resize grow height 10 px or 10 ppt
    bindsym Up              resize shrink height 10 px or 10 ppt
    bindsym Right           resize grow width 10 px or 10 ppt

    bindsym Return          mode "default"
    bindsym Escape          mode "default"
    bindsym $mod+r          mode "default"
}
# Enter resize mode
bindsym $mod+r              mode "resize"

# Launcher mode (ensure firejailed app when possible)
mode "launcher" {
    bindsym f               exec /usr/local/bin/firefox
    bindsym c               exec /usr/local/bin/chromium
    bindsym t               exec /usr/local/bin/thunderbird
    bindsym v               exec /usr/local/bin/vlc

    bindsym Return          mode "default"
    bindsym Escape          mode "default"
    bindsym $mod+less       mode "default"
}
# Enter launcher mode
bindsym $mod+less           mode "launcher"

# Lock screen
bindsym $mod+x              exec i3lock

# Keyboard (layout  + custom media keys)
exec --no-startup-id setxkbmap fr
exec --no-startup-id xbindkeys --poll-rc

# Background
exec_always --no-startup-id ~/.fehbg.sh
exec_always --no-startup-id picom  --config ~/.config/picom.conf

# Polybar 
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# Xresources
exec_always --no-startup-id xrdb $HOME/.Xresources

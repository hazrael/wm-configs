#!/usr/bin/env bash

# Sezt wallpaper on main display using feh
feh --no-fehbg --bg-scale "$HOME/Photos/wallpaper.jpg"

#!/usr/bin/env bash

killall -q polybar

echo "---" | tee -a /tmp/polybar-topbar.log
polybar topbar >> /tmp/polybar-topbar.log 2>&1 &

echo "Polybar started"
